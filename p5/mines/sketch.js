var grid;
var rows = 20, cols = 20, w = 20, totalBees = 20;


function setup() {
  createCanvas((rows * w) + 1 , (cols * w) + 1);

  grid = new Array(cols);
  for (var i = 0; i < cols; i++) {
    grid[i] = new Array(rows);
    for (var j = 0; j < rows; j++) {
      grid[i][j] = new Cell(i, j, w);
    }
  }

  //Pick Bees spots
  var placeOptions = [];
  for (var i = 0; i < cols; i++) {
    for (var j = 0; j < rows; j++) {
      placeOptions.push([i,j]);
    }
  }
  for (var n = 0; n < totalBees; n++) {
    var idx = floor(random(placeOptions.length));
    grid[placeOptions[idx][0]][placeOptions[idx][1]].bee = true;

    placeOptions.splice(idx, 1);

  }

  for (var i = 0; i < cols; i++) {
    for (var j = 0; j < rows; j++) {
      grid[i][j].countBees();
    }
  }
}

function draw() {
  background(255);

  for (var i = 0; i < cols; i++) {
    for (var j = 0; j < rows; j++) {
      grid[i][j].show();
    }
  }
}

function gameOver() {
  for (var i = 0; i < cols; i++) {
    for (var j = 0; j < rows; j++) {
      grid[i][j].revealed = true;
    }
  }
}

function mousePressed() {
  for (var i = 0; i < cols; i++) {
    for (var j = 0; j < rows; j++) {
      if (grid[i][j].contains(mouseX, mouseY)) {
        grid[i][j].reveal();

        if (grid[i][j].bee) {
          gameOver();
        }
      }
    }
  }
}