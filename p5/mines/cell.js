function Cell(i, j, w) {
  this.bee = false;
  this.revealed = false;
  this.i = i;
  this.j = j;
  this.x = i * w;
  this.y = j * w;
  this.w = w;
  this.neighborCount = 0;

  this.show = function() {
    stroke(0);
    noFill();

    rect(this.x, this.y, this.w, this.w);

    if (this.revealed) {
      fill(200);
      if (this.bee) {
        ellipse(this.x+this.w*0.5, this.y+this.w*0.5, this.w * 0.5);
      } else {
        rect(this.x, this.y, this.w, this.w);

        if (this.neighborCount > 0) {
          textAlign(CENTER);
          fill(0);
          text(this.neighborCount, this.x + this.w * 0.5, this.y + this.w-6);
        }
      }
    }
  };

  this.contains = function(x,y) {
    return (x > this.x && x < this.x + this.w && y > this.y && y < this.y + this.w);
  };

  this.reveal = function() {
    this.revealed = true;

    if (this.neighborCount == 0) {
      //nachbarn revealen
       this.floodFill();
    }
  };

  this.floodFill = function() {
    for (var xoff = -1; xoff <= 1; xoff++) {
      var i = this.i + xoff;
      if (i < 0 || i >= cols) continue;

      for (var yoff = -1; yoff <= 1; yoff++) {
        var j = this.j + yoff;
        if (j < 0 || j >= rows) continue;

        var neighbor = grid[i][j];
        if (!neighbor.bee && !neighbor.revealed) {
          neighbor.reveal();
        }
      }
    }
  }

  this.countBees = function() {
    if (this.bee) {
      this.neighborCount = -1;
      return;
    }

    var total = 0;
    if (i > 0 && j > 0 && grid[i-1][j-1].bee) {
      total++;
    }
    if (i > 0 && grid[i-1][j].bee) {
      total++;
    }
    if (i > 0 && j < rows-1 && grid[i-1][j+1].bee) {
      total++;
    }

    if (j > 0 && grid[i][j-1].bee) {
      total++;
    }
    if (j < rows-1 && grid[i][j+1].bee) {
      total++;
    }

    if (i < cols-1 && j > 0 && grid[i+1][j-1].bee) {
      total++;
    }
    if (i < cols-1 && grid[i+1][j].bee) {
      total++;
    }
    if (i < cols-1 && j < rows-1 && grid[i+1][j+1].bee) {
      total++;
    }

    this.neighborCount = total;
  }
}