var gridSize = 20;
var numCols;
var numRows;
var s;
var running;
var food = [];
var frameCounter = 0;

function stopGame() {
  running = false;
  food = [];
  s.reset();
  generateFood();
  frameCounter = 0;
}

function setup() {
  createCanvas(600, 600);

  numCols = floor(width/gridSize);
  numRows = floor(height/gridSize);

  s = new Snake();

  frameRate(10);
  running = false;

  generateFood();
}

function generateFood() {
  food.push(new Food());
}

function draw() {
  background(51);
  s.show();

  if (running) {
    frameCounter += 1;

    s.update();
    s.checkPos();

    if ((frameCounter % 50) === 0) {
      generateFood();
    }

    for (var i = food.length - 1; i >= 0; i--) {
      var f = food[i];
      if (dist(f.x, f.y, s.x, s.y) < 1) {
        s.eat();
        food.splice(i, 1);
        if (food.length == 0) {
          generateFood();
        }
      } else {
        f.show();
      }
    }
  }
}

function keyPressed() {
  if (keyCode === UP_ARROW) {
    s.setDir(0, -1);
  } else if (keyCode === DOWN_ARROW) {
    s.setDir(0, 1);
  } else if (keyCode === LEFT_ARROW) {
    s.setDir(-1, 0);
  } else if (keyCode === RIGHT_ARROW) {
    s.setDir(1, 0);
  } else if (keyCode === ENTER) {
    running = true;
    s.reset();
  }
}