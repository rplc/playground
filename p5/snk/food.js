function Food() {
  this.x = floor(random(numCols))*gridSize;
  this.y = floor(random(numRows))*gridSize;

  this.show = function() {
    fill(255, 0, 100);
    rect(this.x, this.y, gridSize, gridSize);
  }
}