function Snake() {

  this.reset = function() {
    this.x = 0;
    this.y = 0;
    this.xspeed = 1;
    this.yspeed = 0;
    this.total = 0;
    this.tail = [];
  }
  this.reset();

  this.show = function() {
    fill(255);
    for (var i = 0; i < this.tail.length; i++) {
      rect(this.tail[i].x, this.tail[i].y, gridSize, gridSize);
    }
    rect(this.x, this.y, gridSize, gridSize);
  }

  this.update = function() {
    if (this.total === this.tail.length) {
      for (var i = 0; i < this.tail.length - 1; i++) {
        this.tail[i] = this.tail[i + 1];
      }
    }
    this.tail[this.total - 1] = createVector(this.x, this.y);

    this.x += (this.xspeed*gridSize);
    this.y += (this.yspeed*gridSize);
  }

  this.setDir = function(x,y) {
    if (this.xspeed === -x || this.yspeed === -y) {
      return false;
    }
    this.xspeed = x;
    this.yspeed = y;
  }

  this.checkPos = function() {
    if (this.x > (width - gridSize) || this.x < 0 || this.y > (height - gridSize) || this.y < 0) {
      stopGame();
      console.log('Out of playing area');
    }

    for (var i = 0; i < this.tail.length - 1; i++) {
      if (dist(this.tail[i].x, this.tail[i].y, this.x, this.y) < 1) {
        stopGame();
      }
    }
  }

  this.eat = function() {
    this.total++;
  }
}