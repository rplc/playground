function Wall() {
  this.x = width;
  this.speed = 2;
  this.gapSize = (random(2)+2)*50;
  this.gapPos = random((height*0.75));
  this.gapTop = this.gapPos - (this.gapSize/2);
  this.gapBottom = this.gapPos + (this.gapSize/2);

  this.update = function() {
    this.x -= 2;
  }

  this.visible = function() {
    return this.x >= 0;
  }

  this.show = function() {
    rect(this.x, 0, 10, this.gapTop);
    rect(this.x, this.gapBottom, 10, height);
  }
}