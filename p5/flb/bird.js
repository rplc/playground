function Bird() {
  this.x = 20;
  this.y = height/2;
  this.gravity = -0.5;
  this.vel = 0;

  this.update = function() {
    this.vel += this.gravity;
    this.vel *= 0.97;
    this.y -= this.vel;

    if (this.y > height) {
      this.y = height;
      this.vel = 0;
    } else if (this.y < 0) {
      this.y = 0;
      this.vel = 0;
    }
  }

  this.show = function() {
    ellipse(this.x, this.y, 15);
  }

  this.up = function() {
    this.vel += 10;
  }

}