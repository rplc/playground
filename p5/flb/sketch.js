var b;
var walls;

function setup() {
  createCanvas(400, 600);
  b = new Bird();
  walls = [new Wall()];
}

function draw() {
  background(51);

  b.update();
  b.show();

  if (frameCount % 80 === 0) {
    walls.push(new Wall());
  }

  for (var i = walls.length - 1; i >= 0; i--) {
    var w = walls[i];
    w.update();

    if (w.visible()) {
      w.show();
    } else {
      walls.splice(i, 1);
    }
  }
}

function keyPressed() {
  if (key === ' ') {
    b.up();
  }
}