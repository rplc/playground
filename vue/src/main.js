import Vue from 'vue'
import Menge from './menge.vue'

new Vue({
  el: 'menge',
  render: h => h(Menge)
})